const express = require("express");

const router = express.Router();

const {getOrderDetail, updateOrderDetail, deleteOrderDetail} = require("../controller/OrderDetailController");

router.get("/:orderDetailId", getOrderDetail);
router.put("/:orderDetailId", updateOrderDetail);
router.delete("/:orderDetailId", deleteOrderDetail);

module.exports = router;