const express = require("express");

const router = express.Router();

const {getAllOrder, getOrder, updateOrder, deleteOrder} = require("../controller/OrderController");
const {createOrderDetail, getAllOrderDetailOfOrder} = require("../controller/OrderDetailController");

router.get("/", getAllOrder)
router.get("/:orderId", getOrder);
router.put("/:orderId", updateOrder);
router.delete("/:orderId", deleteOrder);

router.post("/:orderId/orderdetails/:productId", createOrderDetail);
router.get("/:orderId/orderdetails", getAllOrderDetailOfOrder)

module.exports = router;