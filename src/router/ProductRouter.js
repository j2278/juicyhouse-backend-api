const express = require("express");

const router = express.Router();

const {createProduct, updateProduct, getAllProduct, getProduct, deleteProduct, getLastestProduct} = require("../controller/ProductController");

router.post("/", createProduct);
router.get("/", getAllProduct);
router.get("/lastest", getLastestProduct);

router.put("/:productId", updateProduct);
router.get("/:productId", getProduct);
router.delete("/:productId", deleteProduct);

module.exports = router;