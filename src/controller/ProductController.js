const mongoose = require("mongoose");

const {ProductModel} = require("../model/ProductModel");

function createProduct(req, res) {
    const product = new ProductModel({
        _id: mongoose.Types.ObjectId(), 
        keyword: `${req.body.brand} ${req.body.name} ${req.body.type} ${req.body.capacity} ${req.body.flavor}`,
        name: req.body.name,
        brand: req.body.brand,
        type: req.body.type,
        capacity: req.body.capacity,
        flavor: req.body.flavor,
        productOrigin: req.body.productOrigin,
        imageUrl: req.body.imageUrl,
        buyPrice: req.body.buyPrice,
        promotionPrice: req.body.promotionPrice,
        description: req.body.description,
    })

    product.save()
    .then((data) => {
        return res.status(200).json({
            success: true,
            product: data
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function updateProduct(req, res) {
    const productId = req.params.productId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(productId)){
        ProductModel.findByIdAndUpdate(productId, body)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    productUpdate: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Product Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Product Id is not valid"
        })
    }
}

function getAllProduct(req, res) {
    const limit = req.query.limit;
    const skip = req.query.skip;

    if(limit && skip){
        ProductModel.find()
        .skip(skip)
        .limit(limit)
        .select("_id keyword name type brand capacity flavor productOrigin imageUrl buyPrice promotionPrice description lastest bestSale timeCreated timeUpdated")
        .then((data) => {
            return res.status(200).json({
                success: true,
                productList: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    
    } else {
        ProductModel.find()
        .select("_id keyword name type brand capacity flavor productOrigin imageUrl buyPrice promotionPrice description lastest bestSale timeCreated timeUpdated")
        .then((data) => {
            return res.status(200).json({
                success: true,
                productList: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })    
    }
}

function getProduct(req, res) {
    const productId = req.params.productId;

    if(mongoose.Types.ObjectId.isValid(productId)){
        ProductModel.findById(productId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    product: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Product Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Product Id is not valid"
        })
    }
}

function deleteProduct(req, res) {
    const productId = req.params.productId;

    if(mongoose.Types.ObjectId.isValid(productId)){
        ProductModel.findByIdAndDelete(productId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Product Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Product Id is not valid"
        })
    }
}

function getLastestProduct (req, res) {
    ProductModel.find({lastest: true})
    .select("_id name brand type capacity imageUrl flavor productOrigin bestSale buyPrice promotionPrice lastest description ")
    .then((data) => {
        return res.status(200).json({
            success: true,
            lastestList: data
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

module.exports = {createProduct, updateProduct, getAllProduct, getProduct, deleteProduct, getLastestProduct}