const mongoose = require("mongoose");
const date = new Date();
const {Schema} = mongoose;
const today = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`

const orderSchema = new Schema({
    _id: Schema.Types.ObjectId,
    customer: {
        type: Schema.Types.ObjectId,
        ref: "customers"
    },
    orderValue: {
        type: Number,
        required: true
    },
    requiredDate: {
        type: String,
        required: false
    },
    shippedDate: {
        type: String,
        default: ""
    },
    note: {
        type: String,
    },
    orderStatus: {
        type: Number,
        default: 0
    },
    timeCreated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${today}`
    },
    timeUpdated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${today}`
    }
})

const OrderModel = mongoose.model("orders", orderSchema);

module.exports = {OrderModel};