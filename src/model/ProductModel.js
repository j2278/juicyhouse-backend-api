const mongoose = require("mongoose");

const {Schema} = mongoose;

const date = new Date();

const productSchema = new Schema({
    _id: Schema.Types.ObjectId,
    keyword: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true,
        unique: true
    },
    brand: {
        type: String,
        require: true
    },
    type: {
        type: String,
        require: true
    },
    capacity: {
        type: Number,
        require: true
    },
    flavor: {
        type: String,
        require: true
    },
    productOrigin: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
        require: true
    },
    buyPrice: {
        type: Number,
        require: true
    },
    promotionPrice: {
        type: Number,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    lastest: {
        type: Boolean,
        default: true
    },
    bestSale: {
        type: Boolean,
        default: false
    },
    timeCreated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
    },
    timeUpdated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
    }
})

const ProductModel = mongoose.model("products", productSchema);

module.exports = {ProductModel}