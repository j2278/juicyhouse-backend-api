const express = require("express");
const router = express.Router();

const {createCustomer, getAllCustomer, getCustomer, updateCustomer, deleteCustomer} = require("../controller/CustomerController");
const {createOrder, getAllOrderOfCustomer} = require("../controller/OrderController")

router.post("/", createCustomer);
router.get("/", getAllCustomer);

router.get("/:customerId", getCustomer);
router.put("/:customerId", updateCustomer);
router.delete("/:customerId", deleteCustomer);

router.post("/:customerId/orders", createOrder);
router.get("/:customerId/orders", getAllOrderOfCustomer);

module.exports = router;