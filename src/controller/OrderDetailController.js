const mongoose = require("mongoose");

const {OrderModel} = require("../model/OrderModel");
const {OrderDetailModel} = require("../model/OrderDetailModel");

function createOrderDetail(req, res) {
    const orderId = req.params.orderId;
    const productId = req.params.productId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findById(orderId)
        .then((data) => {
            if(data){
                const orderDetail = new OrderDetailModel({
                    _id: mongoose.Types.ObjectId(),
                    order: data._id,
                    product: productId,
                    quantity: req.body.quantity,
                    priceEach: req.body.priceEach
                })
                orderDetail.save()
                .then((data) => {
                    return res.status(200).json({
                        success: true,
                        newOrderDetail: data
                    })
                })
                .catch((error) => {
                    return res.status(500).json({
                        success: false,
                        error: error.message
                    })
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
            }
        })

    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function getAllOrderDetailOfOrder(req, res) {
    const orderId = req.params.orderId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderDetailModel.find({order: orderId})
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    list: data
                }) 
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function getOrderDetail(req, res) {
    const orderDetailId = req.params.orderDetailId;

    if(mongoose.Types.ObjectId.isValid(orderDetailId)){
        OrderDetailModel.findById(orderDetailId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    orderDetail: data
                }) 
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Detail Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Detail Id is not valid"
        })
    }
}

function updateOrderDetail(req, res) {
    const orderDetailId = req.params.orderDetailId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(orderDetailId)){
        OrderDetailModel.findByIdAndUpdate(orderDetailId, body)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    orderDetailUpdate: data
                }) 
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Detail Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Detail Id is not valid"
        })
    }
}

function deleteOrderDetail(req, res) {
    const orderDetailId = req.params.orderDetailId;

    if(mongoose.Types.ObjectId.isValid(orderDetailId)){
        OrderDetailModel.findByIdAndDelete(orderDetailId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                }) 
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Detail Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Detail Id is not valid"
        })
    }
}


module.exports = {createOrderDetail, getAllOrderDetailOfOrder, getOrderDetail, updateOrderDetail, deleteOrderDetail};