const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 8000;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next(); 
});

const productRouter = require("./src/router/ProductRouter");
const customerRouter = require("./src/router/CustomerRouter");
const orderRouter = require("./src/router/OrderRouter");
const orderDetailRouter = require("./src/router/OrderDetailRouter");

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

app.get("/", (req, res) => {
    res.send(`Shop24h API`)
});

app.listen(port, () => {
    console.log(`App is listening on ${port}`)
});

async function connectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/Shop24h")
}

connectMongoDB()
.then(() => console.log(`Connect to MongoDB successful`))
.catch((error) => console.log(error.message))

app.use("/products", productRouter);
app.use("/customers", customerRouter);
app.use("/orders", orderRouter);
app.use("/orderdetails", orderDetailRouter);

