const mongoose = require("mongoose");
const {CustomerModel} = require("../model/CustomerModel");

function createCustomer(req, res) {
    const customer = new CustomerModel({
        _id: mongoose.Types.ObjectId(),
        fullname: req.body.fullname,
        phone: req.body.phone,
        email: req.body.email,
        address: req.body.address,
        city: req.body.city,
        country: req.body.country,
        avatar: req.body.avatar,
        cartList: req.body.cartList
    })
    customer.save()
    .then((data) => {
        return res.status(200).json({
            success: true,
            newCustomer: data
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getAllCustomer(req, res) {
    CustomerModel.find()
    .select("_id fullname phone email address city country avatar cartList timeCreated timeUpdated")
    .then((data) => {
        return res.status(200).json({
            success: true,
            customerList: data
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getCustomer(req, res) {
    const customerId = req.params.customerId;
    
    if(mongoose.Types.ObjectId.isValid(customerId)){
        CustomerModel.findById(customerId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    customer: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Customer Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Customer Id is not valid"
        })
    }
}

function updateCustomer(req, res) {
    const customerId = req.params.customerId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(customerId)){
        CustomerModel.findByIdAndUpdate(customerId, body)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    customerUpdate: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Customer Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Customer Id is not valid"
        })
    }
}

function deleteCustomer(req, res) {
    const customerId = req.params.customerId;
    
    if(mongoose.Types.ObjectId.isValid(customerId)){
        CustomerModel.findByIdAndDelete(customerId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Customer Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Customer Id is not valid"
        })
    }
}


module.exports = {createCustomer, getAllCustomer, getCustomer, updateCustomer, deleteCustomer};