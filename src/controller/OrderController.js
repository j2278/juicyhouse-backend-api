const mongoose = require("mongoose");
const {OrderModel} = require("../model/OrderModel");
const {CustomerModel} = require("../model/CustomerModel");

function createOrder(req, res) {
    const customerId = req.params.customerId;

    if(mongoose.Types.ObjectId.isValid(customerId)){
        CustomerModel.findById(customerId)
        .then((customer) => {
            if(customer){
                    const order = new OrderModel({
                        _id: mongoose.Types.ObjectId(),
                        customer: customer._id,
                        orderValue: req.body.orderValue,
                        requiredDate: req.body.requiredDate,
                        note: req.body.note,
                    })
                    order.save()
                    .then((data) => {
                        return res.status(200).json({
                            success: true,
                            newOrder: data
                        })
                    })
                    .catch((error) => {
                        return res.status(500).json({
                            success: false,
                            error: error.message
                        })
                    })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Customer Id is not found"
                }) 
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Customer Id is not valid"
        })
    }
}

function getAllOrder(req, res) {
    OrderModel.find()
    .select("_id customer orderValue requiredDate shippedDate note orderStatus timeCreated timeUpdated")
    .then((data) => {
        return res.status(200).json({
            success: true,
            orderList: data
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getOrder(req, res) {
    const orderId = req.params.orderId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findById(orderId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    order: data
                })        
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
        
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function updateOrder(req, res) {
    const orderId = req.params.orderId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findByIdAndUpdate(orderId, body)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    orderUpdate: data
                })        
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
        
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function deleteOrder(req, res) {
    const orderId = req.params.orderId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findByIdAndDelete(orderId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                })        
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
        
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function getAllOrderOfCustomer(req, res){
    const customerId = req.params.customerId;

    if(mongoose.Types.ObjectId.isValid(customerId)){
        OrderModel.find({customer: customerId})
        .select("_id customer orderValue requiredDate shippedDate note orderStatus timeCreated timeUpdated")
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    customerOrders: data
                })        
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Customer Id is not found"
                })
        
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Customer Id is not valid"
        })
    }

}

module.exports = {createOrder, getAllOrder, getOrder, updateOrder, deleteOrder, getAllOrderOfCustomer};