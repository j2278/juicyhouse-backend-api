const mongoose = require("mongoose");
const {Schema} = mongoose;

const orderDetailSchema = new Schema({
    _id: Schema.Types.ObjectId,
    order: {
        type: Schema.Types.ObjectId,
        ref: "orders"
    },
    product: {
        type: Schema.Types.ObjectId,
        ref: "products"
    },
    quantity: {
        type: Number,
        required: true
    },
    priceEach: {
        type: Number,
        required: true
    }
})

const OrderDetailModel = mongoose.model("orderdetails", orderDetailSchema);

module.exports = {OrderDetailModel}