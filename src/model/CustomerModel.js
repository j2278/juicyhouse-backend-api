const mongoose = require("mongoose");
const date = new Date();
const {Schema} = mongoose;

const customerSchema = new Schema({
    _id: Schema.Types.ObjectId,
    fullname: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        default : ""
    },
    email: {
        type: String,
        require: true
    },
    address: {
        type: String,
        default : ""
    },
    city: {
        type: String,
        default : ""
    },
    country: {
        type: String,
        default : ""
    },
    avatar: {
        type: String,
        default: "https://static.thenounproject.com/png/363639-200.png"
    },
    cartList : {
        type: Array,
        default: []
    },
    timeCreated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
    },
    timeUpdated: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
    }
})

const CustomerModel = mongoose.model("customers", customerSchema);

module.exports = {CustomerModel}